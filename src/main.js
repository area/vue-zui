import Vue from 'vue'
import VueResource from "vue-resource"
import VueRouter from "vue-router"
import ConfigRouter from "./router"
import FastClick from "fastclick"
import App from './App'

document.addEventListener('DOMContentLoaded', function() {
  FastClick.attach(document.body);
}, false);

Vue.use(VueResource);
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;
Vue.use(VueRouter);
const router = new VueRouter();
ConfigRouter(router);

const webApp = Vue.extend(App);
router.start(webApp, '#app');




