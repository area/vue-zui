var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractPlugin = require('extract-text-webpack-plugin');
// NodeJS中的Path对象，用于处理目录的对象，提高开发效率。
// 模块导入
module.exports = {
    // 入口文件地址，不需要写完，会自动查找
    entry: {
        "main":'./src/main'
    },
    // 输出
    output: {
        path: path.join(__dirname, './'),
        // 文件地址，使用绝对路径形式
        filename: '[name].js',
        //[name]这里是webpack提供的根据路口文件自动生成的名字
        publicPath: '/'
        // 公共文件生成的地址
    },
    // 服务器配置相关，自动刷新!
    devServer: {
        historyApiFallback: true,
        hot: false,
        inline: true,
        grogress: true,
    },
    // 加载器
    module: {
        // 加载器
        loaders: [
        // 解析.vue文件
            { test: /\.vue$/, loader: 'vue' },
        // 转化ES6的语法
            { test: /\.js$/, loader: 'babel', exclude: /node_modules/ },
        // 编译css并自动添加css前缀
            { test: /\.scss$/,loader: ExtractPlugin.extract('style', 'css!autoprefixer!sass')},
            { test: /\.css$/,loader: ExtractPlugin.extract('style', 'css!autoprefixer')},
        // 图片转化，小于8K自动转化为base64的编码
            { test: /\.(png|jpg|gif)$/, loader: 'url-loader?limit=8192'},
        //html模板编译？
            { test: /\.(html|tpl)$/, loader: 'html-loader' },
        ]
    },
    // .vue的配置。需要单独出来配置
    vue: {
        loaders: {
            sass: ExtractPlugin.extract('vue-style-loader', 'css-loader!sass-loader'),
            html:'html-loader'
        }
    },
    // 转化成es5的语法
    // babel: {
    //     presets: ['es2015'],
    //     plugins: ['transform-runtime']
    // },
    resolve: {
        // require时省略的扩展名，如：require('module') 不需要module.js
        extensions: ['', '.js', '.vue'],
        // 别名，可以直接使用别名来代表设定的路径以及其他
        alias: {
            lib:path.join(__dirname,'./src/lib'),
            store:path.join(__dirname,'./src/store'),   
            filter: path.join(__dirname, './src/filters'),
            directives:path.join(__dirname,'./src/directives'),
            components: path.join(__dirname, './src/components')
        }
    },
    plugins:[
        new ExtractPlugin ('[name].min.css',{allChunks: true}),
        new HtmlWebpackPlugin({
              title: 'VUI',
              filename:'index.html',
              template: 'index.html',
              chunks:["main"],
              hash:true
        })
    ]
};